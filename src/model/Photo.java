package model;

/**
 * @author Dev & Harshil
 */
import java.io.Serializable;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javafx.scene.image.Image;

public class Photo implements Serializable {

	private static final long serialVersionUID = 1L;

	private transient Image photo;

	private String location;

	private String photoName;

	private String caption;

	private String fileType;

	private ArrayList<Tag> tags;

	private ArrayList<Album> albums;

	private Date date;

	/**
	 * constructor for photo
	 */
	public Photo() {
		this.date = Calendar.getInstance().getTime();
		photo = null;
		this.tags = new ArrayList<Tag>();
	}

	/**
	 * image constructor
	 * 
	 * @param photo
	 */
	public Photo(Image photo) {
		this.photo = photo;
		this.tags = new ArrayList<Tag>();
	}

	/**
	 * gets photo
	 * 
	 * @return
	 */
	public Image getPhoto() {
		return this.photo;
	}

	/**
	 * sets photo
	 * 
	 * @param image
	 */
	public void setPhoto(Image image) {
		this.photo = image;
	}

	/**
	 * gets location
	 * 
	 * @return
	 */
	public String getLocation() {
		return this.location;
	}

	/**
	 * sets location
	 * 
	 * @param location
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * gets photo name
	 * 
	 * @return
	 */
	public String getPhotoName() {
		return this.photoName;
	}

	/**
	 * sets photoname from the String
	 * 
	 * @param photoName
	 */
	public void setPhotoName(String photoName) {
		this.photoName = photoName;
	}

	/**
	 * gets caption
	 * 
	 * @return
	 */
	public String getCaption() {
		return this.caption;
	}

	/**
	 * sets photo caption from String
	 * 
	 * @param caption
	 */
	public void setCaption(String caption) {
		this.caption = caption;
	}

	/**
	 * gets file type
	 * 
	 * @return
	 */
	public String getFileType() {
		return this.fileType;
	}

	/**
	 * sets file type based on String
	 * 
	 * @param fileType
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 * gets arrayList of tags
	 * 
	 * @return
	 */
	public ArrayList<Tag> getTags() {
		return this.tags;
	}

	/**
	 * sets tags based on inputed tags
	 * 
	 * @param tags
	 */
	public void setTags(ArrayList<Tag> tags) {
		this.tags = tags;
	}

	/**
	 * 
	 * @return arraylist of albums
	 */
	public ArrayList<Album> getAlbums() {
		return this.albums;
	}

	/**
	 * sets albums on Albums
	 * 
	 * @param albums
	 */
	public void setAlbums(ArrayList<Album> albums) {
		this.albums = albums;
	}

	/**
	 * gets date
	 * 
	 * @return
	 */
	public Date getDate() {
		return this.date;
	}

//	public void addAlbum() {
//
//		// TODO: Implement add album
//	}

	/**
	 * Temp adding tags
	 * 
	 * @param name
	 * @param value
	 */
	public void addTag(String name, String value) {
		if (name == null || value == null) {
			return;
		}
		for (int i = 0; i < tags.size(); i++) {
			if (tags.get(i).getTagName().equalsIgnoreCase(name.toLowerCase())
					&& tags.get(i).getTagValue().equalsIgnoreCase(value.toLowerCase())) {
				return;
			}
		}
		tags.add(new Tag(name, value));
		return;
	}

	/**
	 * temp return tags
	 * 
	 * @param tag
	 */
	public void removeTag(Tag tag) {
		if (tag.getTagName() == null || tag.getTagValue() == null) {
			return;
		}
		for (int i = 0; i < tags.size(); i++) {
			if (tags.get(i).getTagName().equalsIgnoreCase(tag.getTagName().toLowerCase())
					&& tags.get(i).getTagValue().equalsIgnoreCase(tag.getTagValue().toLowerCase())) {
				tags.remove(i);
				return;

			}
		}
	}

}
