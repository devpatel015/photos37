package model;

import java.io.Serializable;

/**
 * 
 * @author Dev & Harshil
 *
 */
public class Tag implements Serializable {

	private String tagName;

	private String tagValue;

	/**
	 * constructor for Tag
	 * 
	 * @param tagName
	 * @param tagValue
	 */
	public Tag(String tagName, String tagValue) {

		this.tagName = tagName;
		this.tagValue = tagValue;
	}

	/**
	 * gets tag
	 * 
	 * @return
	 */
	public String getTag() {
		return this.tagName + ", " + this.tagValue;
	}

	/**
	 * gets Tag name
	 * 
	 * @return
	 */
	public String getTagName() {
		return this.tagName;
	}

	/**
	 * sets tag name based on input
	 * 
	 * @param tagName
	 */
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	/**
	 * gets tag value
	 * 
	 * @return
	 */
	public String getTagValue() {
		return this.tagValue;
	}

	/**
	 * sets tag value based on input
	 * 
	 * @param tagValue
	 */
	public void setTagValue(String tagValue) {
		this.tagValue = tagValue;
	}
}
