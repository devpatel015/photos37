package model;

/**
 * @author Dev & Harshil
 */
import java.io.Serializable;
import java.util.ArrayList;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;

public class Album implements Serializable {

	private static final long serialVersionUID = 1L;

	private String albumName;

	private ArrayList<Photo> photos;

	/**
	 * constructor for album
	 */
	public Album() {

		this.photos = new ArrayList<Photo>();
		this.albumName = albumName;
	}

	/**
	 * gets albums name
	 * 
	 * @return
	 */
	public String getAlbumName() {
		return this.albumName;
	}

	/**
	 * sets album name
	 * 
	 * @param albumName
	 */
	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	/**
	 * return arraylist of photos
	 * 
	 * @return
	 */
	public ArrayList<Photo> getPhotos() {
		return this.photos;
	}

	/**
	 * gets the size
	 * 
	 * @return
	 */
	public int getAlbumSize() {
		return photos.size();
	}

	/**
	 * renames
	 * 
	 * @param newName
	 */
	public void renameAlbum(String newName) {
		this.albumName = newName;

	}

	/**
	 * sets photos from ArrayList
	 * 
	 * @param photos
	 */
	public void setPhotos(ArrayList<Photo> photos) {
		this.photos = photos;
	}

	/**
	 * temp add Photos
	 * 
	 * @param photo
	 */
	public void addPhoto(Photo photo) {
		if (photos == null) {
			photos = new ArrayList<Photo>();
		}

		// Scan through the photos in the album
		for (int i = 0; i < photos.size(); i++) {

			// Check to see if there are any other photos with the same name
			if (photos.get(i).getPhotoName().equalsIgnoreCase(photo.getPhotoName())) {

				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Name already exists");
				Optional<ButtonType> result = alert.showAndWait();
				if (result.get() == ButtonType.OK) {
					alert.close();
				}
				return;
			}
		}
		photos.add(photo);
	}

	/**
	 * temp delete photo
	 * 
	 * @param photoName
	 */
	public void deletePhoto(String photoName) {

		if (photos.size() == 0) {
			Alert alert2 = new Alert(AlertType.ERROR);
			alert2.setTitle("No photos in this album.");
			Optional<ButtonType> z = alert2.showAndWait();
			if (z.get() == ButtonType.OK) {
				alert2.close();
			}
		}

		for (int i = 0; i < photos.size(); i++) {
			if (photos.get(i).getPhotoName().equalsIgnoreCase(photoName)) {
				photos.remove(i);
				return;
			}
		}

		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Photo not found.");
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) {
			alert.close();
		}
	}

}
